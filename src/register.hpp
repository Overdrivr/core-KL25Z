#ifndef REGISTER_HPP_
#define REGISTER_HPP_

typedef enum {
  readonly,
  readwrite,
  reserved
} mode mode;

template <uint32_t baseaddr, uint8_t count, mode m>

class Register {
public:
  // Unprotected get/set. For portability
  uint32_t & operator ()();

  // Protected set
  void set(uint8_t index, bool bit);

  // Direct flags set/reset
  void set(uint32_t flags);
  void reset(uint32_t flags);

  // Protected get
  void get(uint8_t index, bool bit);

  // SW-only register events notification
  void onset((void callback)(uint32_t baseaddr, uint32_t value));
  void onreset((void callback)(uint32_t baseaddr, uint32_t value));
  void onchange((void callback)(uint32_t baseaddr, uint32_t value));
};

#endif
