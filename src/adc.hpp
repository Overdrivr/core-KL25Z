#ifndef ADC_HPP_
#define ADC_HPP_

#include "register.hpp"

namespace adc {
  /* SC1 constants */
  const uint32_t avg_4 0x0010;
  /* SC1 register */
  static Register<0x00002F, 2, readwrite> SC1;
  
  /* SC2 constants */
  /* SC2 registers */
  static Register<0x00002F, 1, readwrite> SC2;
}
