# Modern system librairies for microcontrollers

This is an experiment for a C++ system library for the NXP KL25Z microcontroller.

This library aims to offer a safer and more modern interface to the hardware
registers of the device.


# Comparisons with the current model
Currently, all microcontrollers ship with a C system library, that provides
access to the hardware registers for reading or writing data to
microcontroller peripherals.

Such libraries take the following form:

```
/* ADC - Register accessors */
#define ADC_R_REG(base,index)                    ((base)->R[index])
#define ADC_R_COUNT                              2
#define ADC_CV1_REG(base)                        ((base)->CV1)
#define ADC_CV2_REG(base)                        ((base)->CV2)
```

Thoses librairies do the job, but also come with some limitations:
* Error-prone : Setting and writing bits at a specific location in the
register involves boolean operations, easy to write wrong.
* Testing : Abstracting those libraries to validate low-level API is impossible.
They are defined as macro so it is absolutely impossible to mock them in any
way.
